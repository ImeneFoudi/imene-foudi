package tp_ed;


public class CoutJointure {

	public double CalculerCoutAcces(double nbr_page_R, int nbr_tuple) {
		return nbr_page_R*(1-Math.exp((-nbr_page_R)/nbr_tuple));
	}
	
	public double CalculerCoutLecture(int val_dis, double degree, int nbr_bitmap, double page_sys , int nbr_tuple) {
		return Math.log(val_dis)-1+(val_dis/(degree-1))+nbr_bitmap*(nbr_tuple/(8*page_sys));
	}
	
	public double CalculerCoutIndex(double cout_acces, double cout_lecture)
	{
		return cout_acces+cout_lecture;
	}

}
