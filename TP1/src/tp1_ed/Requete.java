package tp1_ed;

public class Requete {
	
	String [] attribut, table, condition, expGrp, expOrd;
	int max_length = 1000;
	int ind_attr, ind_table, ind_cond, ind_expG, ind_expO;
	
	public Requete ()
	{
		attribut = new String [max_length];
		table = new String [max_length];
		condition = new String [max_length];
		expGrp = new String[max_length];
		expOrd = new String [max_length];
		
	}
	
	public void setAttribut (String [] nom_attribut)
	{
		this.attribut= nom_attribut;
		
	}
	
	public void setTable (String [] nom_table)
	{
		table = nom_table;
		
	}
	
	public void setCondition (String [] nom_Cond)
	{
		condition= nom_Cond;
		
	}
	
	public void setExpressionGrp (String [] nom_exp_grp)
	{
		expGrp= nom_exp_grp;
	}
	
	public void setExpressionOrd (String [] nom_exp_ord)
	{
		expOrd = nom_exp_ord;
		
	}
	
	public String [] getAttribut ()
	{
		return attribut;
	}
	
	public String [] getTable ()
	{
		return table;
	}
	
	public String [] getCondition ()
	{
		return condition;
	}
	
	public String [] getExpGrp ()
	{
		return expGrp;
	}
	
	public String [] getExpOrd ()
	{
		return expOrd;
	}

}
